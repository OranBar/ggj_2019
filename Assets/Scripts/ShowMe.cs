﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;

public static class ShowMeManager
{
    public static bool showAid = true;
}

public class ShowMe : MonoBehaviour
{
    void Awake()
    {
        if (ShowMeManager.showAid == false)
        {
            this.GetComponent<SpriteRenderer>().enabled = (false);
        }
        else
        {
            this.GetComponent<SpriteRenderer>().enabled = (true);
        }

    }
}
