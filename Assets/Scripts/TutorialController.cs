﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour
{
    [SerializeField] private GameObject[] tutorials = new GameObject[2];
    private int counter;

    private void Awake()
    {
        counter = 0;
        tutorials[0].SetActive(true);
    }

    public void BackToMenu(string level)
    {
        SceneManager.LoadScene(level);
    }

    public void NextTutorial()
    {
        tutorials[counter].SetActive(false);
        counter++;
        if (counter > 1)
        {
            counter = 0;
        }
        tutorials[counter].SetActive(true);
    }

    public void PreviousTutorial()
    {
        tutorials[counter].SetActive(false);
        counter--;
        if (counter < 0)
        {
            counter = 1;
        }
        tutorials[counter].SetActive(true);
    }
}
