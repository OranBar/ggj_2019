﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour {

	//declares public variables to control player movements;
	public float moveForce = 20f;
	public float jumpForce = 700f;
	public float maxVelocity = 4f;

	//declares bool var to check if player is on ground;
	private bool grounded;

	//declear private vars for rigid body and animator component to add force to rigid body and switch animation states;
	private Rigidbody2D myBody;
	private Animator anim;

	private bool moveLeft, moveRight;

	//initialization before the start method;
	void Awake() {
		//calls the function in the awake method;
		InitializeVariables ();
//		GameObject.Find ("Jump Button").GetComponent<Button> ().onClick.AddListener (() => Jump ());
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// FixedUpdate is called once every 2/3 frames;
	void FixedUpdate () {
		PlayerWalkKeyboard ();
//		PlayerWalkJoystick ();
		
	}

	//inizializes the variable to get the rigid body and animator component;
	void InitializeVariables() {
		myBody = GetComponent <Rigidbody2D> ();
		anim = GetComponent<Animator> ();
	}

//	public void SetMoveLeft (bool moveLeft) {
//		this.moveLeft = moveLeft;
//		this.moveRight = !moveLeft;
//	}
//
//	public void StopMoving () {
//		this.moveLeft = false;
//		this.moveRight = false;
//	}
//
//	public void Jump() {
//	
//		if (grounded) {
//			grounded = false;
//			myBody.AddForce (new Vector2(0, jumpForce));
//		}
//	
//	}


//	void PlayerWalkJoystick () {
//		float forceX = 0f;
//		float vel = Mathf.Abs (myBody.velocity.x);
//
//		if (moveRight) {
//		
//			//condition to see if player is moving by comparing the input value h;
//
//			if (vel < maxVelocity) {
//				//to add a bit more controll of the player momevemt when he's in air;
//				if (grounded) {
//					forceX = moveForce;
//
//				} else {
//					forceX = moveForce * 1.1f;
//
//				}
//			}
//
//			//gets the localScale of the p[layer and assign to var scale;
//			Vector3 scale = transform.localScale;
//			//assign scale X to be 1 to face the player right;
//			scale.x = 1f;
//			//reassign the value to the trasform.scale;
//			transform.localScale = scale;
//			//set the animation to be true to make the state machine switch;
//			anim.SetBool ("Walk", true);
//		
//		} else if (moveLeft) {
//		
//			if (vel < maxVelocity) {
//				//to add a bit more controll of the player momevemt when he's in air;
//				if (grounded) {
//					forceX = -moveForce;
//
//				} else {
//					forceX = -moveForce * 1.1f;
//
//				}
//			}
//
//			//gets the localScale of the p[layer and assign to var scale;
//			Vector3 scale = transform.localScale;
//			//assign scale X to be 1 to face the player right;
//			scale.x = -1f;
//			//reassign the value to the trasform.scale;
//			transform.localScale = scale;
//			//set the animation to be true to make the state machine switch;
//			anim.SetBool ("Walk", true);
//		
//		} else {
//		
//			anim.SetBool ("Walk", false);
//		
//		}
//
//		myBody.AddForce (new Vector2(forceX, 0));
//	}

	void PlayerWalkKeyboard () {
		//declear 2 variables for player movement intially set to 0;
		float forceX = 0f;
		float forceY = 0f;

		// gets the absolute value (always positive, number withour signs) of the rigid body velocity x; 
		float vel = Mathf.Abs (myBody.velocity.x);

		// gets inputs for keyboards arrows, A,D numpad horizontally, output 1 and -1;
		float h = Input.GetAxisRaw ("Horizontal");

		//condition to see if player is moving by comparing the input value h;
		if (h > 0) {
			if (vel < maxVelocity) {
				//to add a bit more controll of the player momevemt when he's in air;
				if (grounded) {
					forceX = moveForce;

				} else {
					forceX = moveForce * 1.1f;

				}
			}

			//gets the localScale of the p[layer and assign to var scale;
			Vector3 scale = transform.localScale;
			//assign scale X to be 1 to face the player right;
			scale.x = 1f;
			//reassign the value to the trasform.scale;
			transform.localScale = scale;
			//set the animation to be true to make the state machine switch;
			anim.SetBool ("Walk", true);

		//same as above but for the left;
		} else if (h < 0) {
			if (vel < maxVelocity) {
				if (grounded) {
					forceX = -moveForce;
				} else {
					forceX = -moveForce * 1.1f;;
				}
			}

			Vector3 scale =	transform.localScale;
			scale.x = -1f;
			transform.localScale = scale;
			anim.SetBool ("Walk", true);

		//set walk to false if player is not moving;
		} else if (h == 0) {
			anim.SetBool ("Walk", false);
		}

		//condition to jump getting the input keyboard space; check the boolean variable to see if can jump again and add force to Y axis;
		if (Input.GetKey (KeyCode.Space)) {
			if (grounded) {
				grounded = false;
				forceY = jumpForce;
			}
		}

		// add force to rigid body using vector 2 for x and y following the conditions results;
		myBody.AddForce (new Vector2(forceX, forceY));

	}

	//check collision with the player collider2D finding tags, if it finds the right tag set boolean grounded to true to allow jumping again;
	void OnCollisionEnter2D (Collision2D target) {
		if (target.gameObject.tag == "Ground") {
			grounded = true;
		}
	}

	//function to make the player jump when interact with a function written in bouncer class; it takes 1argument;
	public void BouncePlayer (float force) {
		if (grounded) {
			grounded = false;
			myBody.AddForce (new Vector2(0, force));
		}
	}


}//PlayerScript
