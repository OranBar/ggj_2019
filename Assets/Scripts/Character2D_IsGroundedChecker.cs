﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character2D_IsGroundedChecker : MonoBehaviour
{
    public CharacterController2D controller;
   
    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Pavimento"))
        {
            controller.isGrounded = true;
            Debug.Log("Grounded is true");
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Pavimento")) {
            var wasGrounded = controller.isGrounded;
            if (!wasGrounded){
                controller.OnLandEvent.Invoke();
                controller.isGrounded = false;
                Debug.Log("Grounded is false");
            }
        }
    }
}
