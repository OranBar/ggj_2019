﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.Serialization;

public class Thrower : MonoBehaviour
{
    [SerializeField] private GameObject launcherStartingPoint;
    [SerializeField] private float launchSpeed = 2f;
    [SerializeField] private float launchCooldown = 0.2f;
    [FormerlySerializedAs("throwButtonMapping")]
    [SerializeField] private string throwControllerMapping;
    [SerializeField] private KeyCode throwKeyboardMapping;
    
    [SerializeField] private string throwButtonMapping;
    [SerializeField] private float kickbackForce = 1f;

    private bool canThrowThings;
    private bool isAxisInUse;
    private bool IsTouchingThrowable => throwableThatIAmTouching != null;
    private GameObject throwableThatIAmTouching;
    
    //We get this from ourselves or parent. That's where I expect them to be.
    //In case of emergency, set to public without auto property
    [AutoParent] private PlayerMovement playerMovement;
    [AutoParent] private Animator playerAnimator;
    //---------------    


    private void Awake()
    {
        Assert.IsNotNull(launcherStartingPoint);
    }

    private void Start()
    {
        canThrowThings = true;
    }
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Throwable" && canThrowThings)
        {
            throwableThatIAmTouching = other.gameObject;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (IsTouchingThrowable)
        {
            throwableThatIAmTouching = null;
        }
    }


    private void Update()
    {
        if (Input.GetAxisRaw(throwControllerMapping) <= 0.1f)
        {
            isAxisInUse = false;
        }
        
        if (IsTouchingThrowable && IsMappedButtonPressed())
        {
            Throw(throwableThatIAmTouching);
        }
    }

    public bool IsMappedButtonPressed()
    {
        bool result = throwControllerMapping.Contains("LT") ? UseAxisAsButton(throwControllerMapping) : Input.GetButtonDown(throwControllerMapping);

        return result || Input.GetKeyDown(throwKeyboardMapping);
    }

    private IEnumerator CoolDownThrower()
    {
        yield return new WaitForSeconds(launchCooldown);
        canThrowThings = true;
    }

    private void Throw(GameObject other)
    {
        var rigidBody = other.GetComponentInParent<Rigidbody2D>();
        var throwableProperties = other.GetComponentInParent<Throwable>();

        if (IsAlreadyThrowned(rigidBody))
        {
            if (throwableProperties != null && throwableProperties.IsRelaunchable)
            {
                rigidBody.velocity = Vector2.zero;
                ExecuteThrow(throwableProperties, rigidBody);
            }
        }
        else
        {
            playerMovement.Kickback(kickbackForce);
            ExecuteThrow(throwableProperties, rigidBody);
        }
    }

    private void ExecuteThrow(Throwable throwable, Rigidbody2D rigidBody)
    {
        throwable.Launch(gameObject);
        playerAnimator.SetTrigger(AnimationControllerVariables.IsThrowing.ToString());
        rigidBody.AddForce(launcherStartingPoint.transform.right * launchSpeed, ForceMode2D.Impulse);
    }

    private static bool IsAlreadyThrowned(Rigidbody2D rigidBody)
    {
        return rigidBody.velocity.magnitude != 0.0f;
    }

    private bool UseAxisAsButton(string axisName)
    {

        if (Input.GetAxisRaw(axisName) > 0.1f)
        {
            if (isAxisInUse == false)
            {
                // Call your event function here.
                isAxisInUse = true;
                return true;
            }
        }

        return false;
    }
}
