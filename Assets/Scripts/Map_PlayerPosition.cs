﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Map_PlayerPosition : MonoBehaviour
{

    public Transform p1Pos, p2Pos;
    public Transform p1Start, p1End, p2Start, p2End;

    public Slider p1Slider, p2Slider;
  
    // Update is called once per frame
    void Update()
    {
        float p1Pos_normalized = Mathf.InverseLerp(p1Start.position.x, p1End.position.x, p1Pos.position.x);
        float p2Pos_normalized = Mathf.InverseLerp(p2Start.position.x, p2End.position.x, p2Pos.position.x);

        p1Slider.normalizedValue = p1Pos_normalized + 0.01f;
        p2Slider.normalizedValue = p2Pos_normalized + 0.01f;
    }
}
