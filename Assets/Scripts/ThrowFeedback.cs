﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowFeedback : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D collider)
    {
        var throwable = collider.GetComponent<Throwable>();
        if (throwable != null)
        {
            Debug.Log("CanThrow");
        }
    }
    
    
}
