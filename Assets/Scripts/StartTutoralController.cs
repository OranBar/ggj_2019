﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartTutoralController : MonoBehaviour
{
    [SerializeField] private GameObject panel;
   
    public void StartGame()
    {
        panel.SetActive(false);
        Time.timeScale = 1f;
        
    }
}
