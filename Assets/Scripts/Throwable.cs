﻿using UnityEngine;

public class Throwable : MonoBehaviour
{
  [SerializeField] private bool isRelaunchable;
  [SerializeField] private float launchSpeed;
  [SerializeField] private float pushBackForce;

  public bool IsRelaunchable => isRelaunchable;
  public float LaunchSpeed => launchSpeed;
  public float PushBackForce => pushBackForce;

  private bool isFlying;

  public bool IsFlying => isFlying;

  public void Start()
  {
  }
  
  public void Launch(GameObject myThrower)
  {
    Physics2D.IgnoreCollision(gameObject.GetComponent<Collider2D>(), myThrower.gameObject.GetComponent<Collider2D>());

    isFlying = true;

    this.gameObject.AddComponent<ConstantRotation>().anglePerSecond = new Vector3(0, 0, 360);
  }

  public void OnCollisionEnter2D(Collision2D collision)
  {
    if (collision.gameObject.tag == "Border")
      Destroy(gameObject);
  }
}
