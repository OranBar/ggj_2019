﻿using System.Collections;
using System.Collections.Generic;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.UI;

public class SetShowRadius : MonoBehaviour
{

    public void Set(bool show)
    {
        ShowMeManager.showAid = show;
        FindObjectsOfType<ShowMe>().ForEach(go => go.gameObject.GetComponent<SpriteRenderer>().enabled = (show));
    }
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Toggle>().isOn = ShowMeManager.showAid;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
