﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonBehavior : MonoBehaviour {

    public string NextScene;
    public void PressButton()
    {
        Time.timeScale = 0f;
        SceneManager.LoadScene(NextScene);
    }
    public void CloseGame()
    {
        Application.Quit();
    }
}
