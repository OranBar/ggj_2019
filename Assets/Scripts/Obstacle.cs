﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    public float SpeedMultiplier = 0.5f;
    public float SpeedEffectDuration = 2f;
    private AudioSource audioSource;

    public bool isABooster = false;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        ExecuteCollision(other.gameObject);
    }

    private void ExecuteCollision(GameObject other)
    {
        var playerMovement = other.gameObject.GetComponent<PlayerMovement>();
        if (playerMovement != null)
        {
            if(audioSource!= null)
                audioSource.Play();

            ExecuteCollisionWithObstacle(playerMovement);
        }
    }

    private void ExecuteCollisionWithObstacle(PlayerMovement playerMovement)
    {
        var throwableProperties = GetComponent<Throwable>();
        var hitForce = (throwableProperties != null && throwableProperties.IsFlying)
            ? throwableProperties.PushBackForce
            : 0.0f;

        playerMovement.HitTaken(hitForce, SpeedMultiplier, SpeedEffectDuration);
        DisableObject();
    }

    private void DisableObject()
    {
        gameObject.GetComponent<Rigidbody2D>().simulated = false;
        gameObject.GetComponent<Collider2D>().enabled = false;
        gameObject.GetComponent<SpriteRenderer>().enabled = false;
        gameObject.SetActive(false);
    }
}
