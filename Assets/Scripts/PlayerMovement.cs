﻿using NaughtyAttributes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Random = System.Random;


public class PlayerMovement : MonoBehaviour
{
    public bool isRunningRight = true;

    private float horizontalMove;

    [Auto] private CharacterController2D controller;
    [Auto] private Animator animator;
    [Auto] private ShieldPowerupEnabler shieldEnabler;
    private AudioSource audioSource;
    private GameManager gameManager;
    [FormerlySerializedAs("axixControlName")]
    [BoxGroup("Controller Input")]
    public string axisControlName;
    [BoxGroup("Controller Input")]
    public string jumpControlName;
    [BoxGroup("Controller Input")]
    public string crouchControlName;
    [BoxGroup("Controller Input")]
    public string shieldControlName;

    [BoxGroup("Keyboard Input")]
    public string axis_kb;
    [BoxGroup("Keyboard Input")]
    public KeyCode jump_kb;
    [BoxGroup("Keyboard Input")]
    public KeyCode crouch_kb;
    [BoxGroup("Keyboard Input")]
    public KeyCode shield_kb;

    [BoxGroup("Autorun")]
    [SerializeField] private bool isAutorunning;
    [BoxGroup("Autorun")]
    public List<float> speeds;
    [BoxGroup("Autorun")]
    [SerializeField]
    private int currSpeedIndex;
    [BoxGroup("Autorun")]
    public float timeToIncreaseSpeedIndex = 1f;

    [BoxGroup("Player controlled movement (no Autorun)")]
    public float runSpeed = 28f;
    [BoxGroup("Player controlled movement (no Autorun)")]
    [SerializeField] private float HittedSpeedModifier = 0.7f;
    [BoxGroup("Player controlled movement (no Autorun)")]
    [SerializeField] private float timeBeforeRemoveingHitMalus = 0.4f;

    [SerializeField] private List<AudioClip> hitSounds;


    private bool jump;
    private bool crouch;
    private float speedModifier = 1.0f;
    private int hitTakens;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        gameManager = FindObjectOfType<GameManager>();
        if (isAutorunning)
        {
            horizontalMove = GetStandardHorizontalMovement();
            //horizontalMove = Input.GetAxisRaw(axisControlName);

            StartCoroutine(IncreaseSpeedEveryXSeconds(timeToIncreaseSpeedIndex));
        }

    }

    private IEnumerator IncreaseSpeedEveryXSeconds(float seconds)
    {
        while (gameManager.IsGameRunning && isAutorunning)
        {
            yield return new WaitForSeconds(seconds);
            IncreaseSpeed();
        }

    }

    //Auto Move
    private float GetStandardHorizontalMovement()
    {
        var movementValue = runSpeed;
        if (!isRunningRight)
            movementValue = -movementValue;

        return movementValue;
    }

    // Update is called once per frame
    internal void Update()
    {
        horizontalMove = isAutorunning ? GetStandardHorizontalMovement() : GetHorizontalMovementFromPlayerController();
        animator.SetFloat(AnimationControllerVariables.RunSpeed.ToString(), Mathf.Abs(horizontalMove));

        if (gameManager.IsGameRunning)
        {
            if (isRunningRight && horizontalMove < 0)
                horizontalMove = 0;
            if (!isRunningRight && horizontalMove > 0)
                horizontalMove = 0;

            Handlejump();
            HandleCrouch();
            HandleShield();
        }
    }

    private float GetHorizontalMovementFromPlayerController()
    {
        var controllerValue = Input.GetAxisRaw(axisControlName);
        var keyboardValue = Input.GetAxisRaw(axis_kb);

        if (controllerValue < -0.2f || controllerValue > 0.2f)
            return controllerValue;
        else
            return keyboardValue;
    }

    private void HandleCrouch()
    {
        if (Input.GetButtonDown(crouchControlName) || Input.GetKeyDown(crouch_kb))
        {
            animator.SetBool(AnimationControllerVariables.IsSliding.ToString(), true);
            crouch = true;

            if (GetComponent<CharacterController2D>().isGrounded == false)
            {
                animator.SetTrigger(AnimationControllerVariables.JumpAndSlide.ToString());
            }
        }
        else if (Input.GetButtonUp(crouchControlName) || Input.GetKeyUp(crouch_kb))
        {
            animator.SetBool(AnimationControllerVariables.IsSliding.ToString(), false);
            crouch = false;
        }
    }

    private void Handlejump()
    {
        if (Input.GetButtonDown(jumpControlName) || Input.GetKeyDown(jump_kb))
        {
            if (!crouch)
            {
                jump = true;
                animator.SetBool(AnimationControllerVariables.IsJumping.ToString(), jump);
            }
        }
    }

    private void HandleShield()
    {
        if ((Input.GetButtonDown(shieldControlName) || Input.GetKeyDown(shield_kb)) && shieldEnabler.CanEnableShield())
            EnableShield();
        else if (Input.GetButtonUp(shieldControlName) || Input.GetKeyUp(shield_kb))
            DisableShield();
        else if (Input.GetButton(shieldControlName) || Input.GetKey(shield_kb) && !shieldEnabler.CanEnableShield())
            DisableShield();
    }

    private void DisableShield()
    {
        animator.SetBool(AnimationControllerVariables.HasShield.ToString(), false);
        shieldEnabler.DisableShield();
    }

    private void EnableShield()
    {
        animator.SetBool(AnimationControllerVariables.HasShield.ToString(), true);
        shieldEnabler.EnableShield();
    }

    #region automovement methods

    public void IncreaseSpeed()
    {
        Debug.Log("Increase: from " + currSpeedIndex);
        currSpeedIndex = Math.Min(speeds.Count - 1, currSpeedIndex + 1);
    }

    public void DecreaseSpeed()
    {
        currSpeedIndex = Math.Max(0, currSpeedIndex - 1);
    }

    #endregion

    public void OnLandedAfterJump()
    {
        animator.SetBool(AnimationControllerVariables.IsJumping.ToString(), false);
    }

    void FixedUpdate()
    {
        if (hitTakens <= 0)
            speedModifier = 1.0f;
        else
            speedModifier = 1.0f - HittedSpeedModifier;

        if (gameManager.IsGameRunning)
        {
            var speed = GetHorizontalMovementFromPlayerController() * runSpeed * speedModifier;
            if (!isRunningRight && isAutorunning)
                speed = -speed;

            controller.Move(speed * Time.fixedDeltaTime, crouch, jump);
            jump = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Goal")
            gameManager.GameOver(gameObject);
    }

    public void HitTaken(float hitForce, float speedReductionMultiplier, float speedReductionEffectDuration)
    {
        if (isAutorunning)
            DecreaseSpeed();
        else
        {
            hitTakens += 1;
            speedModifier = HittedSpeedModifier;

            StartCoroutine(RemoveHitTakenAfterTime(timeBeforeRemoveingHitMalus));
        }

        if (audioSource != null && hitSounds != null && hitSounds.Count > 0)
        {
            var toPlay = hitSounds.ElementAt(new Random().Next(0, hitSounds.Count));
            audioSource.PlayOneShot(toPlay);
        }

        animator.SetTrigger(AnimationControllerVariables.IsHit.ToString());

        if (hitForce > 0)
        {
            var hitDirection = GetHitDirection();
            GetComponent<Rigidbody2D>().AddForce(hitDirection * hitForce, ForceMode2D.Impulse);
        }
    }

    private IEnumerator RemoveHitTakenAfterTime(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        hitTakens = Math.Max(0, hitTakens - 1);
    }

    private Vector2 GetHitDirection()
    {
        var hitDirection = isRunningRight ? Vector2.left : Vector2.right;
        return hitDirection;
    }

    internal void Kickback(float kickbackForce)
    {
        var hitDirection = GetHitDirection();
        GetComponent<Rigidbody2D>().AddForce(hitDirection * kickbackForce, ForceMode2D.Impulse);
    }
}