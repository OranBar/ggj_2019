﻿public enum AnimationControllerVariables
{
    Idle,
    RunSpeed,
    IsSliding,
    IsJumping,
    IsHit,
    IsWinner,
    IsLoser,
    IsThrowing,
    HasShield,
    JumpAndSlide,
}
