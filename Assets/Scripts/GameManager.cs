using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using Assert = UnityEngine.Assertions.Assert;

public class GameManager : MonoBehaviour
{
    [SerializeField] private TMP_Text WinText;
    [SerializeField] private TMP_Text ElapsedTimeText;

    [SerializeField] private bool isGameRunning = true;
    [SerializeField] private List<GameObject> PlayerShields;
    [SerializeField] private List<GameObject> Players;

    [SerializeField] private GameObject tutorialCanvas;

    public Canvas pauseCanvas;
    //    [SerializeField] private KeyCode pauseKey;

    public bool IsGameRunning => isGameRunning;

    private void Awake()
    {
        Assert.IsNotNull(ElapsedTimeText);
        Assert.IsNotNull(WinText);
        Assert.IsNotNull(PlayerShields);
        Assert.AreEqual(PlayerShields.Count, 2);
        Assert.IsNotNull(Players);
        Assert.AreEqual(Players.Count, 2);

        tutorialCanvas.SetActive(true);
    }

    private void Start()
    {
        WinText.text = "";
        PlayerShields.ForEach(o => o.SetActive(false));
    }

    private void Update()
    {
        if (isGameRunning)
            ElapsedTimeText.text = Time.timeSinceLevelLoad.ToString("00:00:00");

        if (Input.GetButtonDown("Start") || Input.GetKeyDown(KeyCode.Escape))
        {
            if (isGameRunning) 
                PauseGame();
            else
                UnpauseGame();
        }
    }

    private void UnpauseGame()
    {
        pauseCanvas.gameObject.SetActive(false);
        isGameRunning = true;
        Time.timeScale = 1.0f;
    }

    private void PauseGame()
    {
        pauseCanvas.gameObject.SetActive(true);
        isGameRunning = false;
        Time.timeScale = 0f;
    }

    public void GameOver(GameObject winner)
    {
        var loser = Players.First(p => p.gameObject != winner);
        winner.GetComponent<Animator>().SetTrigger(AnimationControllerVariables.IsWinner.ToString());
        loser.GetComponent<Animator>().SetTrigger(AnimationControllerVariables.IsLoser.ToString());

        winner.GetComponent<Animator>().SetBool(AnimationControllerVariables.HasShield.ToString(), false);
        loser.GetComponent<Animator>().SetBool(AnimationControllerVariables.HasShield.ToString(), false);


        WinText.text = $"{winner.name} wins the game!";
        WinText.gameObject.transform.parent.gameObject.SetActive(true);
        isGameRunning = false;

        winner.GetComponent<Rigidbody2D>().simulated = false;
        loser.GetComponent<Rigidbody2D>().simulated = false;

        //StartCoroutine(WaitSecondsBeforeCredits(3));
    }

    private IEnumerator WaitSecondsBeforeCredits(int secondsToWait)
    {
        yield return new WaitForSeconds(secondsToWait);
        SceneManager.LoadScene("Credits");
    }

    public void Restart()
    {
        Time.timeScale = 0f;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}