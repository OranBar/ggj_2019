﻿using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

public class ShieldPowerupEnabler : MonoBehaviour
{
   [SerializeField] private GameObject ShieldToEnable;
   [SerializeField] private float ShieldDuration = 2.0f;
   [SerializeField] private float shieldAmount = 5f;
   [SerializeField] private Slider shieldSlider;

   public float ShieldAmount => shieldAmount;

   private bool isEnabled = false;

   void OnAwake()
   {
      Assert.IsNotNull(ShieldToEnable);
   }

   void Start()
   {
      shieldSlider.maxValue = ShieldAmount;
      shieldSlider.value = ShieldAmount;
   }

   private void Update()
   {
      if (isEnabled)
      {
         shieldAmount -= Time.deltaTime;

         shieldSlider.value = shieldAmount;

         if (shieldAmount <= 0)
            DisableShield();
      }

   }

   public void EnableShield()
   {
      if (shieldAmount > 0)
      {
         ShieldToEnable.GetComponent<Shield>().InitPosition();

         isEnabled = true;
         ShieldToEnable.gameObject.SetActive(true);
      }
   }

   public void DisableShield()
   {
      isEnabled = false;
      ShieldToEnable.gameObject.SetActive(false);
   }

   public bool CanEnableShield()
   {
      return ShieldAmount > 0f;
   }
}
