﻿using System;
using Pixelplacement;
using UnityEngine;
using Random = UnityEngine.Random;

public class Shield : MonoBehaviour
{
  [SerializeField] private GameObject AttachedTo;
  [SerializeField] private float xdistance = 8f;
  [SerializeField] private float ydistance = 0.3f;


  void Update()
  {
    SetShieldPositionToAttachedObject();
  }

  private void SetShieldPositionToAttachedObject()
  {
    this.transform.position = AttachedTo.gameObject.transform.position + new Vector3(xdistance, ydistance, 0);
  }

  void OnCollisionEnter2D(Collision2D other)
  {

    var rb = other.gameObject.GetComponent<Rigidbody2D>();
    var col = other.gameObject.GetComponent<Collider2D>();

    col.enabled = false;
    SimulateObjectThrowAway(rb);
  }

  private void SimulateObjectThrowAway(Rigidbody2D rb)
  {
    Vector3 rdmVec = new Vector3(xdistance * Random.Range(10, 20f), Random.Range(20, 80), 0);
    rb.AddTorque(180f);
    rb.AddForce(rdmVec, ForceMode2D.Impulse);
    Tween.LocalScale(rb.transform, Vector3.zero, 0.8f, 0f, Tween.EaseIn);
  }

  public void InitPosition()
  {
    SetShieldPositionToAttachedObject();
  }
}
